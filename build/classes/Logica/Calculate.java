/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.util.ArrayList;

/**
 *
 * @author ingcharlie7
 */
public class Calculate {

    
    public double [] ProcesarValoresFAC(ArrayList<Double> encabezado){
        
        
        Double matriz[][] = new Double[23][24];
        //A
        matriz[0][0] = encabezado.get(0);
        //B
        matriz[0][1] = (+encabezado.get(1) - matriz[0][0] * encabezado.get(0)) / (1 - matriz[0][0] * matriz[0][0]);
        matriz[1][1] = (+matriz[0][0] - matriz[0][1] * matriz[0][0]);
        //C
        matriz[0][2] = (+(encabezado.get(2) - (matriz[1][1] * encabezado.get(1) + matriz[0][1] * encabezado.get(0))) / (1 - (matriz[1][1] * encabezado.get(0) + matriz[0][1] * encabezado.get(1))));
        matriz[1][2] = (+matriz[1][1] - matriz[0][2] * matriz[0][1]);
        matriz[2][2] = (+matriz[0][1] - matriz[0][2] * matriz[1][1]);
        //D
        matriz[0][3] = (+(encabezado.get(3) - (matriz[1][2] * encabezado.get(2) + matriz[2][2] * encabezado.get(1) + matriz[0][2] * encabezado.get(0))) / (1 - (matriz[1][2] * encabezado.get(0) + matriz[2][2] * encabezado.get(1) + matriz[0][2] * encabezado.get(2))));
        matriz[1][3] = (+matriz[1][2] - matriz[0][3] * matriz[0][2]);
        matriz[2][3] = (+matriz[2][2] - matriz[0][3] * matriz[2][2]);
        matriz[3][3] = (+matriz[0][2] - matriz[0][3] * matriz[1][2]);
        //E
        matriz[0][4] = (+(encabezado.get(4) - (matriz[1][3] * encabezado.get(3) + matriz[2][3] * encabezado.get(2) + matriz[3][3] * encabezado.get(1) + matriz[0][3] * encabezado.get(0))) / (1 - (matriz[1][3] * encabezado.get(0) + matriz[2][3] * encabezado.get(1) + matriz[3][3] * encabezado.get(2) + matriz[0][3] * encabezado.get(3))));
        matriz[1][4] = (+matriz[1][3] - matriz[0][4] * matriz[0][3]);
        matriz[2][4] = (+matriz[2][3] - matriz[0][4] * matriz[3][3]);
        matriz[3][4] = (+matriz[3][3] - matriz[0][4] * matriz[2][3]);
        matriz[4][4] = (+matriz[0][3] - matriz[0][4] * matriz[1][3]);
        //F
        matriz[0][5] = (+(encabezado.get(5) - (matriz[1][4] * encabezado.get(4) + matriz[2][4] * encabezado.get(3) + matriz[3][4] * encabezado.get(2) + matriz[4][4] * encabezado.get(1) + matriz[0][4] * encabezado.get(0))) / (1 - (matriz[1][4] * encabezado.get(0) + matriz[2][4] * encabezado.get(1) + matriz[3][4] * encabezado.get(2) + matriz[4][4] * encabezado.get(3) + matriz[0][4] * encabezado.get(4))));
        matriz[1][5] = (+matriz[1][4] - matriz[0][5] * matriz[0][4]);
        matriz[2][5] = (+matriz[2][4] - matriz[0][5] * matriz[4][4]);
        matriz[3][5] = (+matriz[3][4] - matriz[0][5] * matriz[3][4]);
        matriz[4][5] = (+matriz[4][4] - matriz[0][5] * matriz[2][4]);
        matriz[5][5] = (+matriz[0][4] - matriz[0][5] * matriz[1][4]);

        matriz[0][6] = (+(encabezado.get(6) - (matriz[1][5] * encabezado.get(5) + matriz[2][5] * encabezado.get(4) + matriz[3][5] * encabezado.get(3) + matriz[4][5] * encabezado.get(2) + matriz[5][5] * encabezado.get(1) + matriz[0][5] * encabezado.get(0))) / (1 - (matriz[1][5] * encabezado.get(0) + matriz[2][5] * encabezado.get(1) + matriz[3][5] * encabezado.get(2) + matriz[4][5] * encabezado.get(3) + matriz[5][5] * encabezado.get(4) + matriz[0][5] * encabezado.get(5))));
        matriz[1][6] = (+matriz[1][5] - matriz[0][6] * matriz[0][5]);
        matriz[1][6] = (+matriz[1][5] - matriz[0][6] * matriz[0][5]);
        matriz[2][6] = (+matriz[2][5] - matriz[0][6] * matriz[5][5]);
        matriz[3][6] = (+matriz[3][5] - matriz[0][6] * matriz[4][5]);
        matriz[4][6] = (+matriz[4][5] - matriz[0][6] * matriz[3][5]);
        matriz[5][6] = (+matriz[5][5] - matriz[0][6] * matriz[2][6]);
        matriz[6][6] = (+matriz[0][5] - matriz[0][6] * matriz[1][5]);

        //H
        matriz[0][7] = (+(encabezado.get(7) - (matriz[1][6] * encabezado.get(6) + matriz[2][6] * encabezado.get(5) + matriz[3][6] * encabezado.get(4) + matriz[4][6] * encabezado.get(3) + matriz[5][6] * encabezado.get(2) + matriz[6][6] * encabezado.get(1) + matriz[0][6] * encabezado.get(0))) / (1 - (matriz[1][6] * encabezado.get(0) + matriz[2][6] * encabezado.get(1) + matriz[3][6] * encabezado.get(2) + matriz[4][6] * encabezado.get(3) + matriz[5][6] * encabezado.get(4) + matriz[6][6] * encabezado.get(5) + matriz[0][6] * encabezado.get(6))));
        matriz[1][7] = (+matriz[1][6] - matriz[0][7] * matriz[0][6]);
        matriz[2][7] = (+matriz[2][6] - matriz[0][7] * matriz[6][6]);
        matriz[3][7] = (+matriz[3][6] - matriz[0][7] * matriz[5][6]);
        matriz[4][7] = (+matriz[4][6] - matriz[0][7] * matriz[4][6]);
        matriz[5][7] = (+matriz[5][6] - matriz[0][7] * matriz[3][6]);
        matriz[6][7] = (+matriz[6][6] - matriz[0][7] * matriz[2][6]);
        matriz[7][7] = (+matriz[0][6] - matriz[0][7] * matriz[1][6]);
        //I
        matriz[0][8] = (+(encabezado.get(8) - (matriz[1][7] * encabezado.get(7) + matriz[2][7] * encabezado.get(6) + matriz[3][7] * encabezado.get(5) + matriz[4][7] * encabezado.get(4) + matriz[5][7] * encabezado.get(3) + matriz[6][7] * encabezado.get(2) + matriz[7][7] * encabezado.get(1) + matriz[0][7] * encabezado.get(0))) / (1 - (matriz[1][7] * encabezado.get(0) + matriz[2][7] * encabezado.get(1) + matriz[3][7] * encabezado.get(2) + matriz[4][7] * encabezado.get(3) + matriz[5][7] * encabezado.get(4) + matriz[6][7] * encabezado.get(5) + matriz[7][7] * encabezado.get(6) + matriz[0][7] * encabezado.get(7))));
        matriz[1][8] = (+matriz[1][7] - matriz[0][8] * matriz[0][7]);
        matriz[2][8] = (+matriz[2][7] - matriz[0][8] * matriz[7][7]);
        matriz[3][8] = (+matriz[3][7] - matriz[0][8] * matriz[6][7]);
        matriz[4][8] = (+matriz[4][7] - matriz[0][8] * matriz[5][7]);
        matriz[5][8] = (+matriz[5][7] - matriz[0][8] * matriz[4][7]);
        matriz[6][8] = (+matriz[6][7] - matriz[0][8] * matriz[3][7]);
        matriz[7][8] = (+matriz[7][7] - matriz[0][8] * matriz[2][7]);
        matriz[8][8] = (+matriz[0][7] - matriz[0][8] * matriz[1][7]);

        //J
        matriz[0][9] = (+(encabezado.get(9) - (matriz[1][8] * encabezado.get(8) + matriz[2][8] * encabezado.get(7) + matriz[3][8] * encabezado.get(6) + matriz[4][8] * encabezado.get(5) + matriz[5][8] * encabezado.get(4) + matriz[6][8] * encabezado.get(3) + matriz[7][8] * encabezado.get(2) + matriz[8][8] * encabezado.get(1) + matriz[0][8] * encabezado.get(0))) / (1 - (matriz[1][8] * encabezado.get(0) + matriz[2][8] * encabezado.get(1) + matriz[3][8] * encabezado.get(2) + matriz[4][8] * encabezado.get(3) + matriz[5][8] * encabezado.get(4) + matriz[6][8] * encabezado.get(5) + matriz[7][8] * encabezado.get(6) + matriz[8][8] * encabezado.get(7) + matriz[0][8] * encabezado.get(8))));
        matriz[1][9] = (+matriz[1][8] - matriz[0][9] * matriz[0][8]);
        matriz[2][9] = (+matriz[2][8] - matriz[0][9] * matriz[8][8]);
        matriz[3][9] = (+matriz[3][8] - matriz[0][9] * matriz[7][8]);
        matriz[4][9] = (+matriz[4][8] - matriz[0][9] * matriz[6][8]);
        matriz[5][9] = (+matriz[5][8] - matriz[0][9] * matriz[5][8]);
        matriz[6][9] = (+matriz[6][8] - matriz[0][9] * matriz[4][8]);
        matriz[7][9] = (+matriz[7][8] - matriz[0][9] * matriz[3][8]);
        matriz[8][9] = (+matriz[8][8] - matriz[0][9] * matriz[2][8]);
        matriz[9][9] = (+matriz[0][8] - matriz[0][9] * matriz[1][8]);
        //K
        matriz[0][10] = (+(encabezado.get(10) - (matriz[1][9] * encabezado.get(9) + matriz[2][9] * encabezado.get(8) + matriz[3][9] * encabezado.get(7) + matriz[4][9] * encabezado.get(6) + matriz[5][9] * encabezado.get(5) + matriz[6][9] * encabezado.get(4) + matriz[7][9] * encabezado.get(3) + matriz[8][9] * encabezado.get(2) + matriz[9][9] * encabezado.get(1) + matriz[0][9] * encabezado.get(0))) / (1 - (matriz[1][9] * encabezado.get(0) + matriz[2][9] * encabezado.get(1) + matriz[3][9] * encabezado.get(2) + matriz[4][9] * encabezado.get(3) + matriz[5][9] * encabezado.get(4) + matriz[6][9] * encabezado.get(5) + matriz[7][9] * encabezado.get(6) + matriz[8][9] * encabezado.get(7) + matriz[9][9] * encabezado.get(8) + matriz[0][9] * encabezado.get(9))));
        matriz[1][10] = (+matriz[1][9] - matriz[0][10] * matriz[0][9]);
        matriz[2][10] = (+matriz[2][9] - matriz[0][10] * matriz[9][9]);
        matriz[3][10] = (+matriz[3][9] - matriz[0][10] * matriz[8][9]);
        matriz[4][10] = (+matriz[4][9] - matriz[0][10] * matriz[7][9]);
        matriz[5][10] = (+matriz[5][9] - matriz[0][10] * matriz[6][9]);
        matriz[6][10] = (+matriz[6][9] - matriz[0][10] * matriz[5][9]);
        matriz[7][10] = (+matriz[7][9] - matriz[0][10] * matriz[4][9]);
        matriz[8][10] = (+matriz[8][9] - matriz[0][10] * matriz[3][9]);
        matriz[9][10] = (+matriz[9][9] - matriz[0][10] * matriz[2][9]);
        matriz[10][10] = (+matriz[0][9] - matriz[0][10] * matriz[1][9]);
        //L
        matriz[0][11] = (+(encabezado.get(11) - (matriz[1][10] * encabezado.get(10) + matriz[2][10] * encabezado.get(9) + matriz[3][10] * encabezado.get(8) + matriz[4][10] * encabezado.get(7) + matriz[5][10] * encabezado.get(6) + matriz[6][10] * encabezado.get(5) + matriz[7][10] * encabezado.get(4) + matriz[8][10] * encabezado.get(3) + matriz[9][10] * encabezado.get(2) + matriz[10][10] * encabezado.get(1) + matriz[0][10] * encabezado.get(0))) / (1 - (matriz[1][10] * encabezado.get(0) + matriz[2][10] * encabezado.get(1) + matriz[3][10] * encabezado.get(2) + matriz[4][10] * encabezado.get(3) + matriz[5][10] * encabezado.get(4) + matriz[6][10] * encabezado.get(5) + matriz[7][10] * encabezado.get(6) + matriz[8][10] * encabezado.get(7) + matriz[9][10] * encabezado.get(8) + matriz[10][10] * encabezado.get(9) + matriz[0][10] * encabezado.get(10))));
        matriz[1][11] = (+matriz[1][10] - matriz[0][11] * matriz[0][10]);
        matriz[2][11] = (+matriz[2][10] - matriz[0][11] * matriz[10][10]);
        matriz[3][11] = (+matriz[3][10] - matriz[0][11] * matriz[9][10]);
        matriz[4][11] = (+matriz[4][10] - matriz[0][11] * matriz[8][10]);
        matriz[5][11] = (+matriz[5][10] - matriz[0][11] * matriz[7][10]);
        matriz[6][11] = (+matriz[6][10] - matriz[0][11] * matriz[6][10]);
        matriz[7][11] = (+matriz[7][10] - matriz[0][11] * matriz[5][10]);
        matriz[8][11] = (+matriz[8][10] - matriz[0][11] * matriz[4][10]);
        matriz[9][11] = (+matriz[9][10] - matriz[0][11] * matriz[3][10]);
        matriz[10][11] = (+matriz[10][10] - matriz[0][11] * matriz[2][10]);
        matriz[11][11] = (+matriz[0][10] - matriz[0][11] * matriz[1][10]);
        //M
        matriz[0][12] = (+(encabezado.get(12) - (matriz[1][11] * encabezado.get(11) + matriz[2][11] * encabezado.get(10) + matriz[3][11] * encabezado.get(9) + matriz[4][11] * encabezado.get(8) + matriz[5][11] * encabezado.get(7) + matriz[6][11] * encabezado.get(6) + matriz[7][11] * encabezado.get(5) + matriz[8][11] * encabezado.get(4) + matriz[9][11] * encabezado.get(3) + matriz[10][11] * encabezado.get(2) + matriz[11][11] * encabezado.get(1) + matriz[0][11] * encabezado.get(0))) / (1 - (matriz[1][11] * encabezado.get(0) + matriz[2][11] * encabezado.get(1) + matriz[3][11] * encabezado.get(2) + matriz[4][11] * encabezado.get(3) + matriz[5][11] * encabezado.get(4) + matriz[6][11] * encabezado.get(5) + matriz[7][11] * encabezado.get(6) + matriz[8][11] * encabezado.get(7) + matriz[9][11] * encabezado.get(8) + matriz[10][11] * encabezado.get(9) + matriz[11][11] * encabezado.get(10) + matriz[0][11] * encabezado.get(11))));
        matriz[1][12] = (+matriz[1][11] - matriz[0][12] * matriz[0][11]);
        matriz[2][12] = (+matriz[2][11] - matriz[0][12] * matriz[11][11]);
        matriz[3][12] = (+matriz[3][11] - matriz[0][12] * matriz[10][11]);
        matriz[4][12] = (+matriz[4][11] - matriz[0][12] * matriz[9][11]);
        matriz[5][12] = (+matriz[5][11] - matriz[0][12] * matriz[8][11]);
        matriz[6][12] = (+matriz[6][11] - matriz[0][12] * matriz[7][11]);
        matriz[7][12] = (+matriz[7][11] - matriz[0][12] * matriz[6][11]);
        matriz[8][12] = (+matriz[8][11] - matriz[0][12] * matriz[5][11]);
        matriz[9][12] = (+matriz[9][11] - matriz[0][12] * matriz[4][11]);
        matriz[10][12] = (+matriz[10][11] - matriz[0][12] * matriz[3][11]);
        matriz[11][12] = (+matriz[11][11] - matriz[0][12] * matriz[2][11]);
        matriz[12][12] = (+matriz[0][11] - matriz[0][12] * matriz[1][11]);
        //N
        matriz[0][13] = (+(encabezado.get(13) - (matriz[1][12] * encabezado.get(12) + matriz[2][12] * encabezado.get(11) + matriz[3][12] * encabezado.get(10) + matriz[4][12] * encabezado.get(9) + matriz[5][12] * encabezado.get(8) + matriz[6][12] * encabezado.get(7) + matriz[7][12] * encabezado.get(6) + matriz[8][12] * encabezado.get(5) + matriz[9][12] * encabezado.get(4) + matriz[10][12] * encabezado.get(3) + matriz[11][12] * encabezado.get(2) + matriz[12][12] * encabezado.get(1) + matriz[0][12] * encabezado.get(0))) / (1 - (matriz[1][12] * encabezado.get(0) + matriz[2][12] * encabezado.get(1) + matriz[3][12] * encabezado.get(2) + matriz[4][12] * encabezado.get(3) + matriz[5][12] * encabezado.get(4) + matriz[6][12] * encabezado.get(5) + matriz[7][12] * encabezado.get(6) + matriz[8][12] * encabezado.get(7) + matriz[9][12] * encabezado.get(8) + matriz[10][12] * encabezado.get(9) + matriz[11][12] * encabezado.get(10) + matriz[12][12] * encabezado.get(11) + matriz[0][12] * encabezado.get(12))));
        matriz[1][13] = (+matriz[1][12] - matriz[0][13] * matriz[0][12]);
        matriz[2][13] = (+matriz[2][12] - matriz[0][13] * matriz[12][12]);
        matriz[3][13] = (+matriz[3][12] - matriz[0][13] * matriz[11][12]);
        matriz[4][13] = (+matriz[4][12] - matriz[0][13] * matriz[10][12]);
        matriz[5][13] = (+matriz[5][12] - matriz[0][13] * matriz[9][12]);
        matriz[6][13] = (+matriz[6][12] - matriz[0][13] * matriz[8][12]);
        matriz[7][13] = (+matriz[7][12] - matriz[0][13] * matriz[7][12]);
        matriz[8][13] = (+matriz[8][12] - matriz[0][13] * matriz[6][12]);
        matriz[9][13] = (+matriz[9][12] - matriz[0][13] * matriz[5][12]);
        matriz[10][13] = (+matriz[10][12] - matriz[0][13] * matriz[4][12]);
        matriz[11][13] = (+matriz[11][12] - matriz[0][13] * matriz[3][12]);
        matriz[12][13] = (+matriz[12][12] - matriz[0][13] * matriz[2][12]);
        matriz[13][13] = (+matriz[0][12] - matriz[0][13] * matriz[1][12]);
        //O
        matriz[0][14] = (+(encabezado.get(14) - (matriz[1][13] * encabezado.get(13) + matriz[2][13] * encabezado.get(12) + matriz[3][13] * encabezado.get(11) + matriz[4][13] * encabezado.get(10) + matriz[5][13] * encabezado.get(9) + matriz[6][13] * encabezado.get(8) + matriz[7][13] * encabezado.get(7) + matriz[8][13] * encabezado.get(6) + matriz[9][13] * encabezado.get(5) + matriz[10][13] * encabezado.get(4) + matriz[11][13] * encabezado.get(3) + matriz[12][13] * encabezado.get(2) + matriz[13][13] * encabezado.get(1) + matriz[0][13] * encabezado.get(0))) / (1 - (matriz[1][13] * encabezado.get(0) + matriz[2][13] * encabezado.get(1) + matriz[3][13] * encabezado.get(2) + matriz[4][13] * encabezado.get(3) + matriz[5][13] * encabezado.get(4) + matriz[6][13] * encabezado.get(5) + matriz[7][13] * encabezado.get(6) + matriz[8][13] * encabezado.get(7) + matriz[9][13] * encabezado.get(8) + matriz[10][13] * encabezado.get(9) + matriz[11][13] * encabezado.get(10) + matriz[12][13] * encabezado.get(11) + matriz[13][13] * encabezado.get(12) + matriz[0][13] * encabezado.get(13))));
        matriz[1][14] = (+matriz[1][13] - matriz[0][14] * matriz[0][13]);
        matriz[2][14] = (+matriz[2][13] - matriz[0][14] * matriz[13][13]);
        matriz[3][14] = (+matriz[3][13] - matriz[0][14] * matriz[12][13]);
        matriz[4][14] = (+matriz[4][13] - matriz[0][14] * matriz[11][13]);
        matriz[5][14] = (+matriz[5][13] - matriz[0][14] * matriz[10][13]);
        matriz[6][14] = (+matriz[6][13] - matriz[0][14] * matriz[9][13]);
        matriz[7][14] = (+matriz[7][13] - matriz[0][14] * matriz[8][13]);
        matriz[8][14] = (+matriz[8][13] - matriz[0][14] * matriz[7][13]);
        matriz[9][14] = (+matriz[9][13] - matriz[0][14] * matriz[6][13]);
        matriz[10][14] = (+matriz[10][13] - matriz[0][14] * matriz[5][13]);
        matriz[11][14] = (+matriz[11][13] - matriz[0][14] * matriz[4][13]);
        matriz[12][14] = (+matriz[12][13] - matriz[0][14] * matriz[3][13]);
        matriz[13][14] = (+matriz[13][13] - matriz[0][14] * matriz[2][13]);
        matriz[14][14] = (+matriz[0][13] - matriz[0][14] * matriz[1][13]);

        //P
        matriz[0][15] = (+(encabezado.get(15) - (matriz[1][14] * encabezado.get(14) + matriz[2][14] * encabezado.get(13) + matriz[3][14] * encabezado.get(12) + matriz[4][14] * encabezado.get(11) + matriz[5][14] * encabezado.get(10) + matriz[6][14] * encabezado.get(9) + matriz[7][14] * encabezado.get(8) + matriz[8][14] * encabezado.get(7) + matriz[9][14] * encabezado.get(6) + matriz[10][14] * encabezado.get(5) + matriz[11][14] * encabezado.get(4) + matriz[12][14] * encabezado.get(3) + matriz[13][14] * encabezado.get(2) + matriz[14][14] * encabezado.get(1) + matriz[0][14] * encabezado.get(0))) / (1 - (matriz[1][14] * encabezado.get(0) + matriz[2][14] * encabezado.get(1) + matriz[3][14] * encabezado.get(2) + matriz[4][14] * encabezado.get(3) + matriz[5][14] * encabezado.get(4) + matriz[6][14] * encabezado.get(5) + matriz[7][14] * encabezado.get(6) + matriz[8][14] * encabezado.get(7) + matriz[9][14] * encabezado.get(8) + matriz[10][14] * encabezado.get(9) + matriz[11][14] * encabezado.get(10) + matriz[12][14] * encabezado.get(11) + matriz[13][14] * encabezado.get(12) + matriz[14][14] * encabezado.get(13) + matriz[0][14] * encabezado.get(14))));
        matriz[1][15] = (+matriz[1][14] - matriz[0][15] * matriz[0][14]);
        matriz[2][15] = (+matriz[2][14] - matriz[0][15] * matriz[14][14]);
        matriz[3][15] = (+matriz[3][14] - matriz[0][15] * matriz[13][14]);
        matriz[4][15] = (+matriz[4][14] - matriz[0][15] * matriz[12][14]);
        matriz[5][15] = (+matriz[5][14] - matriz[0][15] * matriz[11][14]);
        matriz[6][15] = (+matriz[6][14] - matriz[0][15] * matriz[10][14]);
        matriz[7][15] = (+matriz[7][14] - matriz[0][15] * matriz[9][14]);
        matriz[8][15] = (+matriz[8][14] - matriz[0][15] * matriz[8][14]);
        matriz[9][15] = (+matriz[9][14] - matriz[0][15] * matriz[7][14]);
        matriz[10][15] = (+matriz[10][14] - matriz[0][15] * matriz[6][14]);
        matriz[11][15] = (+matriz[11][14] - matriz[0][15] * matriz[5][14]);
        matriz[12][15] = (+matriz[12][14] - matriz[0][15] * matriz[4][14]);
        matriz[13][15] = (+matriz[13][14] - matriz[0][15] * matriz[3][14]);
        matriz[14][15] = (+matriz[14][14] - matriz[0][15] * matriz[2][14]);
        matriz[15][15] = (+matriz[0][14] - matriz[0][15] * matriz[1][14]);
        //Q
        matriz[0][16] = (+(encabezado.get(16) - (matriz[1][15] * encabezado.get(15) + matriz[2][15] * encabezado.get(14) + matriz[3][15] * encabezado.get(13) + matriz[4][15] * encabezado.get(12) + matriz[5][15] * encabezado.get(11) + matriz[6][15] * encabezado.get(10) + matriz[7][15] * encabezado.get(9) + matriz[8][15] * encabezado.get(8) + matriz[9][15] * encabezado.get(7) + matriz[10][15] * encabezado.get(6) + matriz[11][15] * encabezado.get(5) + matriz[12][15] * encabezado.get(4) + matriz[13][15] * encabezado.get(3) + matriz[14][15] * encabezado.get(2) + matriz[15][15] * encabezado.get(1) + matriz[0][15] * encabezado.get(0))) / (1 - (matriz[1][15] * encabezado.get(0) + matriz[2][15] * encabezado.get(1) + matriz[3][15] * encabezado.get(2) + matriz[4][15] * encabezado.get(3) + matriz[5][15] * encabezado.get(4) + matriz[6][15] * encabezado.get(5) + matriz[7][15] * encabezado.get(6) + matriz[8][15] * encabezado.get(7) + matriz[9][15] * encabezado.get(8) + matriz[10][15] * encabezado.get(9) + matriz[11][15] * encabezado.get(10) + matriz[12][15] * encabezado.get(11) + matriz[13][15] * encabezado.get(12) + matriz[14][15] * encabezado.get(13) + matriz[15][15] * encabezado.get(14) + matriz[0][15] * encabezado.get(15))));
        matriz[1][16] = (+matriz[1][15] - matriz[0][16] * matriz[0][15]);
        matriz[2][16] = (+matriz[2][15] - matriz[0][16] * matriz[15][15]);
        matriz[3][16] = (+matriz[3][15] - matriz[0][16] * matriz[14][15]);
        matriz[4][16] = (+matriz[4][15] - matriz[0][16] * matriz[13][15]);
        matriz[5][16] = (+matriz[5][15] - matriz[0][16] * matriz[12][15]);
        matriz[6][16] = (+matriz[6][15] - matriz[0][16] * matriz[11][15]);
        matriz[7][16] = (+matriz[7][15] - matriz[0][16] * matriz[10][15]);
        matriz[8][16] = (+matriz[8][15] - matriz[0][16] * matriz[9][15]);
        matriz[9][16] = (+matriz[9][15] - matriz[0][16] * matriz[8][15]);
        matriz[10][16] = (+matriz[10][15] - matriz[0][16] * matriz[7][15]);
        matriz[11][16] = (+matriz[11][15] - matriz[0][16] * matriz[6][15]);
        matriz[12][16] = (+matriz[12][15] - matriz[0][16] * matriz[5][15]);
        matriz[13][16] = (+matriz[13][15] - matriz[0][16] * matriz[4][15]);
        matriz[14][16] = (+matriz[14][15] - matriz[0][16] * matriz[3][15]);
        matriz[15][16] = (+matriz[15][15] - matriz[0][16] * matriz[2][15]);
        matriz[16][16] = (+matriz[0][15] - matriz[0][16] * matriz[1][15]);

        //R
        matriz[0][17] = (+(encabezado.get(17) - (matriz[1][16] * encabezado.get(16) + matriz[2][16] * encabezado.get(15) + matriz[3][16] * encabezado.get(14) + matriz[4][16] * encabezado.get(13) + matriz[5][16] * encabezado.get(12) + matriz[6][16] * encabezado.get(11) + matriz[7][16] * encabezado.get(10) + matriz[8][16] * encabezado.get(9) + matriz[9][16] * encabezado.get(8) + matriz[10][16] * encabezado.get(7) + matriz[11][16] * encabezado.get(6) + matriz[12][16] * encabezado.get(5) + matriz[13][16] * encabezado.get(4) + matriz[14][16] * encabezado.get(3) + matriz[15][16] * encabezado.get(2) + matriz[16][16] * encabezado.get(1) + matriz[0][16] * encabezado.get(0))) / (1 - (matriz[1][16] * encabezado.get(0) + matriz[2][16] * encabezado.get(1) + matriz[3][16] * encabezado.get(2) + matriz[4][16] * encabezado.get(3) + matriz[5][16] * encabezado.get(4) + matriz[6][16] * encabezado.get(5) + matriz[7][16] * encabezado.get(6) + matriz[8][16] * encabezado.get(7) + matriz[9][16] * encabezado.get(8) + matriz[10][16] * encabezado.get(9) + matriz[11][16] * encabezado.get(10) + matriz[12][16] * encabezado.get(11) + matriz[13][16] * encabezado.get(12) + matriz[14][16] * encabezado.get(13) + matriz[15][16] * encabezado.get(14) + matriz[16][16] * encabezado.get(15) + matriz[0][16] * encabezado.get(16))));
        matriz[1][17] = (+matriz[1][16] - matriz[0][17] * matriz[0][16]);
        matriz[2][17] = (+matriz[2][16] - matriz[0][17] * matriz[16][16]);
        matriz[3][17] = (+matriz[3][16] - matriz[0][17] * matriz[15][16]);
        matriz[4][17] = (+matriz[4][16] - matriz[0][17] * matriz[14][16]);
        matriz[5][17] = (+matriz[5][16] - matriz[0][17] * matriz[13][16]);
        matriz[6][17] = (+matriz[6][16] - matriz[0][17] * matriz[12][16]);
        matriz[7][17] = (+matriz[7][16] - matriz[0][17] * matriz[11][16]);
        matriz[8][17] = (+matriz[8][16] - matriz[0][17] * matriz[10][16]);
        matriz[9][17] = (+matriz[9][16] - matriz[0][17] * matriz[9][16]);
        matriz[10][17] = (+matriz[10][16] - matriz[0][17] * matriz[8][16]);
        matriz[11][17] = (+matriz[11][16] - matriz[0][17] * matriz[7][16]);
        matriz[12][17] = (+matriz[12][16] - matriz[0][17] * matriz[6][16]);
        matriz[13][17] = (+matriz[13][16] - matriz[0][17] * matriz[5][16]);
        matriz[14][17] = (+matriz[14][16] - matriz[0][17] * matriz[4][16]);
        matriz[15][17] = (+matriz[15][16] - matriz[0][17] * matriz[3][16]);
        matriz[16][17] = (+matriz[16][16] - matriz[0][17] * matriz[2][16]);
        matriz[17][17] = (+matriz[0][16] - matriz[0][17] * matriz[1][16]);
        //S
        matriz[0][18] = (+(encabezado.get(18) - (matriz[1][17] * encabezado.get(17) + matriz[2][17] * encabezado.get(16) + matriz[3][17] * encabezado.get(15) + matriz[4][17] * encabezado.get(14) + matriz[5][17] * encabezado.get(13) + matriz[6][17] * encabezado.get(12) + matriz[7][17] * encabezado.get(11) + matriz[8][17] * encabezado.get(10) + matriz[9][17] * encabezado.get(9) + matriz[10][17] * encabezado.get(8) + matriz[11][17] * encabezado.get(7) + matriz[12][17] * encabezado.get(6) + matriz[13][17] * encabezado.get(5) + matriz[14][17] * encabezado.get(4) + matriz[15][17] * encabezado.get(3) + matriz[16][17] * encabezado.get(2) + matriz[17][17] * encabezado.get(1) + matriz[0][17] * encabezado.get(0))) / (1 - (matriz[1][17] * encabezado.get(0) + matriz[2][17] * encabezado.get(1) + matriz[3][17] * encabezado.get(2) + matriz[4][17] * encabezado.get(3) + matriz[5][17] * encabezado.get(4) + matriz[6][17] * encabezado.get(5) + matriz[7][17] * encabezado.get(6) + matriz[8][17] * encabezado.get(7) + matriz[9][17] * encabezado.get(8) + matriz[10][17] * encabezado.get(9) + matriz[11][17] * encabezado.get(10) + matriz[12][17] * encabezado.get(11) + matriz[13][17] * encabezado.get(12) + matriz[14][17] * encabezado.get(13) + matriz[15][17] * encabezado.get(14) + matriz[16][17] * encabezado.get(15) + matriz[17][17] * encabezado.get(16) + matriz[0][17] * encabezado.get(17))));
        matriz[1][18] = (+matriz[1][17] - matriz[0][18] * matriz[0][17]);
        matriz[2][18] = (+matriz[2][17] - matriz[0][18] * matriz[17][17]);
        matriz[3][18] = (+matriz[3][17] - matriz[0][18] * matriz[16][17]);
        matriz[4][18] = (+matriz[4][17] - matriz[0][18] * matriz[15][17]);
        matriz[5][18] = (+matriz[5][17] - matriz[0][18] * matriz[14][17]);
        matriz[6][18] = (+matriz[6][17] - matriz[0][18] * matriz[13][17]);
        matriz[7][18] = (+matriz[7][17] - matriz[0][18] * matriz[12][17]);
        matriz[8][18] = (+matriz[8][17] - matriz[0][18] * matriz[11][17]);
        matriz[9][18] = (+matriz[9][17] - matriz[0][18] * matriz[10][17]);
        matriz[10][18] = (+matriz[10][17] - matriz[0][18] * matriz[9][17]);
        matriz[11][18] = (+matriz[11][17] - matriz[0][18] * matriz[8][17]);
        matriz[12][18] = (+matriz[12][17] - matriz[0][18] * matriz[7][17]);
        matriz[13][18] = (+matriz[13][17] - matriz[0][18] * matriz[6][17]);
        matriz[14][18] = (+matriz[14][17] - matriz[0][18] * matriz[5][17]);
        matriz[15][18] = (+matriz[15][17] - matriz[0][18] * matriz[4][17]);
        matriz[16][18] = (+matriz[16][17] - matriz[0][18] * matriz[3][17]);
        matriz[17][18] = (+matriz[17][17] - matriz[0][18] * matriz[2][17]);
        matriz[18][18] = (+matriz[0][17] - matriz[0][18] * matriz[1][17]);
        //T
        matriz[0][19] = (+(encabezado.get(19) - (matriz[1][18] * encabezado.get(18) + matriz[2][18] * encabezado.get(17) + matriz[3][18] * encabezado.get(16) + matriz[4][18] * encabezado.get(15) + matriz[5][18] * encabezado.get(14) + matriz[6][18] * encabezado.get(13) + matriz[7][18] * encabezado.get(12) + matriz[8][18] * encabezado.get(11) + matriz[9][18] * encabezado.get(10) + matriz[10][18] * encabezado.get(9) + matriz[11][18] * encabezado.get(8) + matriz[12][18] * encabezado.get(7) + matriz[13][18] * encabezado.get(6) + matriz[14][18] * encabezado.get(5) + matriz[15][18] * encabezado.get(4) + matriz[16][18] * encabezado.get(3) + matriz[17][18] * encabezado.get(2) + matriz[18][18] * encabezado.get(1) + matriz[0][18] * encabezado.get(0))) / (1 - (matriz[1][18] * encabezado.get(0) + matriz[2][18] * encabezado.get(1) + matriz[3][18] * encabezado.get(2) * matriz[4][18] * encabezado.get(3) + matriz[5][18] * encabezado.get(4) + matriz[6][18] * encabezado.get(5) + matriz[7][18] * encabezado.get(6) + matriz[8][18] * encabezado.get(7) + matriz[9][18] * encabezado.get(8) + matriz[10][18] * encabezado.get(9) + matriz[11][18] * encabezado.get(10) + matriz[12][18] * encabezado.get(11) + matriz[13][18] * encabezado.get(12) + matriz[14][18] * encabezado.get(13) + matriz[15][18] * encabezado.get(14) + matriz[16][18] * encabezado.get(15) + matriz[17][18] * encabezado.get(16) + matriz[18][18] * encabezado.get(17) + matriz[0][18] * encabezado.get(18))));
        matriz[1][19] = (+matriz[1][18] - matriz[0][19] * matriz[0][18]);
        matriz[2][19] = (+matriz[2][18] - matriz[0][19] * matriz[18][18]);
        matriz[3][19] = (+matriz[3][18] - matriz[0][19] * matriz[17][18]);
        matriz[4][19] = (+matriz[4][18] - matriz[0][19] * matriz[16][18]);
        matriz[5][19] = (+matriz[5][18] - matriz[0][19] * matriz[15][18]);
        matriz[6][19] = (+matriz[6][18] - matriz[0][19] * matriz[14][18]);
        matriz[7][19] = (+matriz[7][18] - matriz[0][19] * matriz[13][18]);
        matriz[8][19] = (+matriz[8][18] - matriz[0][19] * matriz[12][18]);
        matriz[9][19] = (+matriz[9][18] - matriz[0][19] * matriz[11][18]);
        matriz[10][19] = (+matriz[10][18] - matriz[0][19] * matriz[10][18]);
        matriz[11][19] = (+matriz[11][18] - matriz[0][19] * matriz[9][18]);
        matriz[12][19] = (+matriz[12][18] - matriz[0][19] * matriz[8][18]);
        matriz[13][19] = (+matriz[13][18] - matriz[0][19] * matriz[7][18]);
        matriz[14][19] = (+matriz[14][18] - matriz[0][19] * matriz[6][18]);
        matriz[15][19] = (+matriz[15][18] - matriz[0][19] * matriz[5][18]);
        matriz[16][19] = (+matriz[16][18] - matriz[0][19] * matriz[4][18]);
        matriz[17][19] = (+matriz[17][18] - matriz[0][19] * matriz[3][18]);
        matriz[18][19] = (+matriz[18][18] - matriz[0][19] * matriz[2][18]);
        matriz[19][19] = (+matriz[0][18] - matriz[0][19] * matriz[1][18]);
        //U
        matriz[0][20] = (+(encabezado.get(20) - (matriz[1][19] * encabezado.get(19) + matriz[2][19] * encabezado.get(18) + matriz[3][19] * encabezado.get(17) + matriz[4][19] * encabezado.get(16) + matriz[5][19] * encabezado.get(15) + matriz[6][19] * encabezado.get(14) + matriz[7][19] * encabezado.get(13) + matriz[8][19] * encabezado.get(12) + matriz[9][19] * encabezado.get(11) + matriz[10][19] * encabezado.get(10) + matriz[11][19] * encabezado.get(9) + matriz[12][19] * encabezado.get(8) + matriz[13][19] * encabezado.get(7) + matriz[14][19] * encabezado.get(6) + matriz[15][19] * encabezado.get(5) + matriz[16][19] * encabezado.get(4) + matriz[17][19] * encabezado.get(3) + matriz[18][19] * encabezado.get(2) + matriz[19][19] * encabezado.get(1) + matriz[0][19] * encabezado.get(0))) / (1 - (matriz[1][19] * encabezado.get(0) + matriz[2][19] * encabezado.get(1) + matriz[3][19] * encabezado.get(2) + matriz[4][19] * encabezado.get(3) + matriz[5][19] * encabezado.get(4) + matriz[6][19] * encabezado.get(5) + matriz[7][19] * encabezado.get(6) + matriz[8][19] * encabezado.get(7) + matriz[9][19] * encabezado.get(8) + matriz[10][19] * encabezado.get(9) + matriz[11][19] * encabezado.get(10) + matriz[12][19] * encabezado.get(11) + matriz[13][19] * encabezado.get(12) + matriz[14][19] * encabezado.get(13) + matriz[15][19] * encabezado.get(14) + matriz[16][19] * encabezado.get(15) + matriz[17][19] * encabezado.get(16) + matriz[18][19] * encabezado.get(17) + matriz[19][19] * encabezado.get(18) + matriz[0][19] * encabezado.get(19))));
        matriz[1][20] = (+matriz[1][19] - matriz[0][20] * matriz[0][19]);
        matriz[2][20] = (+matriz[2][19] - matriz[0][20] * matriz[19][19]);
        matriz[3][20] = (+matriz[3][19] - matriz[0][20] * matriz[18][19]);
        matriz[4][20] = (+matriz[4][19] - matriz[0][20] * matriz[17][19]);
        matriz[5][20] = (+matriz[5][19] - matriz[0][20] * matriz[16][19]);
        matriz[6][20] = (+matriz[6][19] - matriz[0][20] * matriz[15][19]);
        matriz[7][20] = (+matriz[7][19] - matriz[0][20] * matriz[14][19]);
        matriz[8][20] = (+matriz[8][19] - matriz[0][20] * matriz[13][19]);
        matriz[9][20] = (+matriz[9][19] - matriz[0][20] * matriz[12][19]);
        matriz[10][20] = (+matriz[10][19] - matriz[0][20] * matriz[11][19]);
        matriz[11][20] = (+matriz[11][19] - matriz[0][20] * matriz[10][19]);
        matriz[12][20] = (+matriz[12][19] - matriz[0][20] * matriz[9][19]);
        matriz[13][20] = (+matriz[13][19] - matriz[0][20] * matriz[8][19]);
        matriz[14][20] = (+matriz[14][19] - matriz[0][20] * matriz[7][19]);
        matriz[15][20] = (+matriz[15][19] - matriz[0][20] * matriz[6][19]);
        matriz[16][20] = (+matriz[16][19] - matriz[0][20] * matriz[5][19]);
        matriz[17][20] = (+matriz[17][19] - matriz[0][20] * matriz[4][19]);
        matriz[18][20] = (+matriz[18][19] - matriz[0][20] * matriz[3][19]);
        matriz[19][20] = (+matriz[19][19] - matriz[0][20] * matriz[2][19]);
        matriz[20][20] = (+matriz[0][19] - matriz[0][20] * matriz[1][19]);
        //V
        matriz[0][21] = (+(encabezado.get(21) - (matriz[1][20] * encabezado.get(20) + matriz[2][20] * encabezado.get(19) + matriz[3][20] * encabezado.get(18) + matriz[4][20] * encabezado.get(17) + matriz[5][20] * encabezado.get(16) + matriz[6][20] * encabezado.get(15) + matriz[7][20] * encabezado.get(14) + matriz[8][20] * encabezado.get(13) + matriz[9][20] * encabezado.get(12) + matriz[10][20] * encabezado.get(11) + matriz[11][20] * encabezado.get(10) + matriz[12][20] * encabezado.get(9) + matriz[13][20] * encabezado.get(8) + matriz[14][20] * encabezado.get(7) + matriz[15][20] * encabezado.get(6) + matriz[16][20] * encabezado.get(5) + matriz[17][20] * encabezado.get(4) + matriz[18][20] * encabezado.get(3) + matriz[19][20] * encabezado.get(2) + matriz[20][20] * encabezado.get(1) + matriz[0][20] * encabezado.get(0))) / (1 - (matriz[1][20] * encabezado.get(0) + matriz[2][20] * encabezado.get(1) + matriz[3][20] * encabezado.get(2) + matriz[4][20] * encabezado.get(3) + matriz[5][20] * encabezado.get(4) + matriz[6][20] * encabezado.get(5) + matriz[7][20] * encabezado.get(6) + matriz[8][20] * encabezado.get(7) + matriz[9][20] * encabezado.get(8) + matriz[10][20] * encabezado.get(9) + matriz[11][20] * encabezado.get(10) + matriz[12][20] * encabezado.get(11) + matriz[13][20] * encabezado.get(12) + matriz[14][20] * encabezado.get(13) + matriz[15][20] * encabezado.get(14) + matriz[16][20] * encabezado.get(15) + matriz[17][20] * encabezado.get(16) + matriz[18][20] * encabezado.get(17) + matriz[19][20] * encabezado.get(18) + matriz[20][20] * encabezado.get(19) + matriz[0][20] * encabezado.get(20))));
        matriz[1][21] = (+matriz[1][20] - matriz[0][21] * matriz[0][20]);
        matriz[2][21] = (+matriz[2][20] - matriz[0][21] * matriz[20][20]);
        matriz[3][21] = (+matriz[3][20] - matriz[0][21] * matriz[19][20]);
        matriz[4][21] = (+matriz[4][20] - matriz[0][21] * matriz[18][20]);
        matriz[5][21] = (+matriz[5][20] - matriz[0][21] * matriz[17][20]);
        matriz[6][21] = (+matriz[6][20] - matriz[0][21] * matriz[16][20]);
        matriz[7][21] = (+matriz[7][20] - matriz[0][21] * matriz[15][20]);
        matriz[8][21] = (+matriz[8][20] - matriz[0][21] * matriz[14][20]);
        matriz[9][21] = (+matriz[9][20] - matriz[0][21] * matriz[13][20]);
        matriz[10][21] = (+matriz[10][20] - matriz[0][21] * matriz[12][20]);
        matriz[11][21] = (+matriz[11][20] - matriz[0][21] * matriz[11][20]);
        matriz[12][21] = (+matriz[12][20] - matriz[0][21] * matriz[10][20]);
        matriz[13][21] = (+matriz[13][20] - matriz[0][21] * matriz[9][20]);
        matriz[14][21] = (+matriz[14][20] - matriz[0][21] * matriz[8][20]);
        matriz[15][21] = (+matriz[15][20] - matriz[0][21] * matriz[7][20]);
        matriz[16][21] = (+matriz[16][20] - matriz[0][21] * matriz[6][20]);
        matriz[17][21] = (+matriz[17][20] - matriz[0][21] * matriz[5][20]);
        matriz[18][21] = (+matriz[18][20] - matriz[0][21] * matriz[4][20]);
        matriz[19][21] = (+matriz[19][20] - matriz[0][21] * matriz[3][20]);
        matriz[20][21] = (+matriz[20][20] - matriz[0][21] * matriz[2][20]);
        matriz[21][21] = (+matriz[0][20] - matriz[0][21] * matriz[1][20]);
        //W
        matriz[0][22] = (+(encabezado.get(22) - (matriz[1][21] * encabezado.get(21) + matriz[2][21] * encabezado.get(20) + matriz[3][21] * encabezado.get(19) + matriz[4][21] * encabezado.get(18) + matriz[5][21] * encabezado.get(17) + matriz[6][21] * encabezado.get(16) + matriz[7][21] * encabezado.get(15) + matriz[8][21] * encabezado.get(14) + matriz[9][21] * encabezado.get(13) + matriz[10][21] * encabezado.get(12) + matriz[11][21] * encabezado.get(11) + matriz[12][21] * encabezado.get(10) + matriz[13][21] * encabezado.get(9) + matriz[14][21] * encabezado.get(8) + matriz[15][21] * encabezado.get(7) + matriz[16][21] * encabezado.get(6) + matriz[17][21] * encabezado.get(5) + matriz[18][21] * encabezado.get(4) + matriz[19][21] * encabezado.get(3) + matriz[20][21] * encabezado.get(2) + matriz[21][21] * encabezado.get(1) + matriz[0][21] * encabezado.get(0))) / (1 - (matriz[1][21] * encabezado.get(0) + matriz[2][21] * encabezado.get(1) + matriz[3][21] * encabezado.get(2) * matriz[4][21] * encabezado.get(3) + matriz[5][21] * encabezado.get(4) + matriz[6][21] * encabezado.get(5) + matriz[7][21] * encabezado.get(6) + matriz[8][21] * encabezado.get(7) + matriz[9][21] * encabezado.get(8) + matriz[10][21] * encabezado.get(9) + matriz[11][21] * encabezado.get(10) + matriz[12][21] * encabezado.get(11) + matriz[13][21] * encabezado.get(12) + matriz[14][21] * encabezado.get(13) + matriz[15][21] * encabezado.get(14) + matriz[16][21] * encabezado.get(15) + matriz[17][21] * encabezado.get(16) + matriz[18][21] * encabezado.get(17) + matriz[19][21] * encabezado.get(18) + matriz[20][21] * encabezado.get(19) + matriz[21][21] * encabezado.get(20) + matriz[0][21] * encabezado.get(21))));
        matriz[1][22] = (+matriz[1][21] - matriz[0][22] * matriz[0][21]);
        matriz[2][22] = (+matriz[2][21] - matriz[0][22] * matriz[21][21]);
        matriz[3][22] = (+matriz[3][21] - matriz[0][22] * matriz[20][21]);
        matriz[4][22] = (+matriz[4][21] - matriz[0][22] * matriz[19][21]);
        matriz[5][22] = (+matriz[5][21] - matriz[0][22] * matriz[18][21]);
        matriz[6][22] = (+matriz[6][21] - matriz[0][22] * matriz[17][21]);
        matriz[7][22] = (+matriz[7][21] - matriz[0][22] * matriz[16][21]);
        matriz[8][22] = (+matriz[8][21] - matriz[0][22] * matriz[15][21]);
        matriz[9][22] = (+matriz[9][21] - matriz[0][22] * matriz[14][21]);
        matriz[10][22] = (+matriz[10][21] - matriz[0][22] * matriz[13][21]);
        matriz[11][22] = (+matriz[11][21] - matriz[0][22] * matriz[12][21]);
        matriz[12][22] = (+matriz[12][21] - matriz[0][22] * matriz[11][21]);
        matriz[13][22] = (+matriz[13][21] - matriz[0][22] * matriz[10][21]);
        matriz[14][22] = (+matriz[14][21] - matriz[0][22] * matriz[9][21]);
        matriz[15][22] = (+matriz[15][21] - matriz[0][22] * matriz[8][21]);
        matriz[16][22] = (+matriz[16][21] - matriz[0][22] * matriz[7][21]);
        matriz[17][22] = (+matriz[17][21] - matriz[0][22] * matriz[6][21]);
        matriz[18][22] = (+matriz[18][21] - matriz[0][22] * matriz[5][21]);
        matriz[19][22] = (+matriz[19][21] - matriz[0][22] * matriz[4][21]);
        matriz[20][22] = (+matriz[20][21] - matriz[0][22] * matriz[3][21]);
        matriz[21][22] = (+matriz[21][21] - matriz[0][22] * matriz[2][21]);
        matriz[22][22] = (+matriz[0][21] - matriz[0][22] * matriz[1][21]);
        //X
        matriz[0][23] = (+(encabezado.get(23) - (matriz[1][22] * encabezado.get(22) + matriz[2][22] * encabezado.get(21) + matriz[3][22] * encabezado.get(20) + matriz[4][22] * encabezado.get(19) + matriz[5][22] * encabezado.get(18) + matriz[6][22] * encabezado.get(17) + matriz[7][22] * encabezado.get(16) + matriz[8][22] * encabezado.get(15) + matriz[9][22] * encabezado.get(14) + matriz[10][22] * encabezado.get(13) + matriz[11][22] * encabezado.get(12) + matriz[12][22] * encabezado.get(11) + matriz[13][22] * encabezado.get(10) + matriz[14][22] * encabezado.get(9) + matriz[15][22] * encabezado.get(8) + matriz[16][22] * encabezado.get(7) + matriz[17][22] * encabezado.get(6) + matriz[18][22] * encabezado.get(5) + matriz[19][22] * encabezado.get(4) + matriz[20][22] * encabezado.get(3) + matriz[21][22] * encabezado.get(2) + matriz[22][22] * encabezado.get(1) + matriz[0][22] * encabezado.get(0))) / (1 - (matriz[1][22] * encabezado.get(0) + matriz[2][22] * encabezado.get(1) + matriz[3][22] * encabezado.get(2) + matriz[4][22] * encabezado.get(3) + matriz[5][22] * encabezado.get(4) + matriz[6][22] * encabezado.get(5) + matriz[7][22] * encabezado.get(6) + matriz[8][22] * encabezado.get(7) + matriz[9][22] * encabezado.get(8) + matriz[10][22] * encabezado.get(9) + matriz[11][22] * encabezado.get(10) + matriz[12][22] * encabezado.get(11) + matriz[13][22] * encabezado.get(12) + matriz[14][22] * encabezado.get(13) + matriz[15][22] * encabezado.get(14) + matriz[16][22] * encabezado.get(15) + matriz[17][22] * encabezado.get(16) + matriz[18][22] * encabezado.get(17) + matriz[19][22] * encabezado.get(18) + matriz[20][22] * encabezado.get(19) + matriz[21][22] * encabezado.get(20) + matriz[22][22] * encabezado.get(21) + matriz[0][22] * encabezado.get(22))));
        matriz[1][23] = (+matriz[1][22] - matriz[0][23] * matriz[0][22]);

        
        double [] array = new double[matriz[0].length];
        for (int col = 0; col < matriz[0].length; col++) {
            array[col] = matriz[0][col];
        }
        
        return array;

    }
    
    public double [] ProcesarDatosArima(double [] enc, double promedio){
        double [] array = new double[6];

        array[5] = (enc[17] *(enc[18] + enc[19] + enc[20] + enc[21]) - enc[13] * enc[26] - enc[14] * enc[29] - enc[15] * enc[31] - enc[16] * enc[32])
                    /
                   (enc[22] * (enc[18] + enc[19] +enc[20] + enc[21]) - Math.pow(enc[26], 2) - Math.pow(enc[29], 2) - Math.pow(enc[31], 2) - Math.pow(enc[32], 2));
        
        array[4] = (enc[16] *(enc[18] + enc[19] + enc[20] + enc[22]) - enc[13] * enc[25] - enc[14] * enc[28] - enc[15] * enc[30] - enc[17] * enc[32])
                    /
                   (enc[21] * (enc[18] + enc[19] +enc[20] + enc[22]) - Math.pow(enc[25], 2) - Math.pow(enc[28], 2) - Math.pow(enc[30], 2) - Math.pow(enc[32], 2));
       
        array[3] = (enc[15] *(enc[18] + enc[19] + enc[21] + enc[22]) - enc[13] * enc[24] - enc[14] * enc[27] - enc[16] * enc[30] - enc[17] * enc[31])
                    /
                   (enc[20] * (enc[18] + enc[19] +enc[21] + enc[22]) - Math.pow(enc[24], 2) - Math.pow(enc[27], 2) - Math.pow(enc[30], 2) - Math.pow(enc[31], 2));
        
        array[2] = (enc[14] *(enc[18] + enc[20] + enc[20] + enc[22]) - enc[13] * enc[23] - enc[15] * enc[27] - enc[16] * enc[28] - enc[17] * enc[29])
                    /
                   (enc[19] * (enc[18] + enc[20] +enc[20] + enc[22]) - Math.pow(enc[23], 2) - Math.pow(enc[27], 2) - Math.pow(enc[28], 2) - Math.pow(enc[29], 2));
        
        array[1] = (enc[13] *(enc[19] + enc[20] + enc[20] + enc[22]) - enc[14] * enc[23] - enc[15] * enc[24] - enc[16] * enc[25] - enc[17] * enc[26])
                    /
                   (enc[18] * (enc[19] + enc[20] +enc[20] + enc[22]) - Math.pow(enc[23], 2) - Math.pow(enc[24], 2) - Math.pow(enc[25], 2) - Math.pow(enc[26], 2));
        
        array[0] = promedio *(1 - array[1] - array[2] - array[3] - array[4]);
        
        return array;
    }

}
