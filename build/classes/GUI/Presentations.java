/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kfsarango1
 */
public class Presentations {
    public void LoadTable(DefaultTableModel table, ArrayList<Double> fac, double facp []){
        DropRows(table);
        for (int i = 0; i < fac.size(); i++) {
            table.addRow(new Object[i]);
            table.setValueAt(fac.get(i), i, 0);
            table.setValueAt(facp[i], i, 1);
        }
    }
    
    public double [] LoadTable(DefaultTableModel table, double  matriz [][], int cantidad){
        DropRows(table);
        double [] array = new double[cantidad];
        DataPronosticos(matriz, array, cantidad);
        for (int i = 0; i < array.length; i++) {
                table.addRow(new Object[i]);
                table.setValueAt(array[i], i, 0);
        }
        return array;
    }
    
    public void DataPronosticos(double  matriz [][], double [] array, int cantidad){
        int idxArray = 0;
        for (int i = 4; i < matriz.length; i++) {
            array[idxArray] = matriz[i][1];
            idxArray++;
            if (idxArray == cantidad) {
                return;
            }
        }
    }
    
    public void LoadPrecipitaciones(double [][] matriz, DefaultTableModel table){
        int startRow = matriz.length - 2;
        DropRows(table);
        for (int i = 0; i < matriz[0].length; i++) {
                table.addRow(new Object[i]);
                table.setValueAt(String.valueOf(i + 1), i, 0);
                table.setValueAt("\t"+String.format("%.2f", matriz[startRow][i]) + " %", i, 1);
                table.setValueAt(matriz[startRow + 1][i], i, 2);
                
        }
        
    }
    
    public void LoadTableHidro(DefaultTableModel table, ArrayList<Double> lista){
        DropRows(table);
        for (int i = 0; i < lista.size(); i++) {
                table.addRow(new Object[i]);
                table.setValueAt( (i + 1), i, 0);
                table.setValueAt(lista.get(i), i, 1);
        }
    }
    
    public String TextForHidro(ArrayList<Double> lista, double qi){
        String txt = "";
        String concat = "";
        boolean noHay = true;
        int dia = 1;
        txt += "Valores arriba del Caudal de Inundación (Qi)";
        concat += "\nDía\t|\tPrecipitación";
        concat += "\n------------------------------------------------------------------";
        for (Double val : lista) {
            if (val >= qi) {
                concat += "\n        " + dia +"\t|\t"+String.format("%.2f", val);
                noHay = false;
            }
            dia++;
        }
        if (noHay) {
            txt += "\nNinguno";
        }else{
            txt += concat;
        }
        
        return txt;
        
    }
    
    public void DropRows(DefaultTableModel modelTabla){
        int filas=modelTabla.getRowCount();
            for (int i = 0;filas>i; i++) {
                modelTabla.removeRow(0);
            }
    }
}
